const express = require('express')

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const enterpriseRoutes = require('../routes/v1/restricted/enterprise')
const authenticationRoutes = require('../routes/v1/authentication')

app.use(enterpriseRoutes)
app.use(authenticationRoutes)

module.exports = app

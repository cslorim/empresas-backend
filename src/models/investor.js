'use strict'
module.exports = (sequelize, DataTypes) => {
  const Investor = sequelize.define('investors', {
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    investor_name: {
      allowNull: false,
      type: DataTypes.STRING(128)
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING(128)
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING(60)
    },
    city: {
      allowNull: false,
      type: DataTypes.STRING(64)
    },
    country: {
      allowNull: false,
      type: DataTypes.STRING(64)
    },
    balance: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    photo: {
      allowNull: true,
      type: DataTypes.STRING(255)
    },
    first_access: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    super_angel: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  }, {
    timestamps: false
  })
  Investor.associate = function (models) {
    // associations can be defined here
  }
  return Investor
}

const express = require('express')
const router = express.Router()

router.get('/api/v1/enterprises', (req, res) => {
  res.send('/api/v1/enterprises')
})

module.exports = router

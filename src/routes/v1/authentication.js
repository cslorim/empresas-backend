const express = require('express')
const router = express.Router()

const authenticationController = require('../../controllers/authentication').authentication

router.post('/api/v1/users/auth/sign_in', async (req, res) => authenticationController(req, res))

module.exports = router

'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('investors', [{
      investor_name: 'Teeste Apple',
      email: 'testeapple@ioasys.com.br',
      password: '$2b$10$2ZDRY3DmZkk9pY1FXmYLaua40V1JzwJ0f.Tg7DCFavdQBj4lKBZJK',
      city: 'BH',
      country: 'Brasil',
      balance: 1000000.0,
      photo: null,
      first_access: false,
      super_angel: false
    }], {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('investors', null, {})
  }
}

'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('investors', {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      investor_name: {
        allowNull: false,
        type: Sequelize.STRING(128)
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING(128)
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING(60)
      },
      city: {
        allowNull: false,
        type: Sequelize.STRING(64)
      },
      country: {
        allowNull: false,
        type: Sequelize.STRING(64)
      },
      balance: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      photo: {
        allowNull: true,
        type: Sequelize.STRING(255)
      },
      first_access: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      super_angel: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('investor')
  }
}


const application = require(`./config/server.js`)

const NODE_ENV = process.env.NODE_ENV
const PORT = process.env.PORT || 8080

application.listen(PORT, () => {
  console.log(`\n🚀 Online server at the port: ${PORT}, NODE_ENV=${NODE_ENV}`)
})

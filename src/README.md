# README

Estes documento README tem como objetivo fornecer as informações necessárias execução deste projeto.

### Pré-requisitos ?

- NodeJS 10+;
- NPM 6+;
- Opcionais caso queira usar docker-compose para gerenciar o banco de dados PostgreSQL:
  - Docker 19+;
  - Docker-compose 1.17+.

### Passo a passo para

- Clone o projeto
```bash
$ git clone git@bitbucket.org:cslorim/empresas-backend.git
$ cd empresas-backend
```

- Instalação das dependências do projeto
```bash
$ cd npm install
```

- Opcionalmente caso o banco de dados esteja sendo controlado via docker-compose, inicie via docker-compose
```bash
$ docker-compose up
```

- Crie um banco com o nome "empresas_ioasys"

- Inicie execute as migrates e seeds para popular o banco de dados
```bash
$ npm run db:migrate
$ npm run db:seed
```

- Para iniciar a aplicação em modo desenvolvimento
```bash
$ npm run dev
```

- Para iniciar a aplicação em modo produção (não completamente implementado)
```bash
$ npm start
```

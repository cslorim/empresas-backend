const bcrypt = require('bcrypt')
const { sequelize, Sequelize } = require('../models/index')
const Investor = require('../models/investor')(sequelize, Sequelize)
const jwt = require('jsonwebtoken')
const dateFns = require('date-fns')

const getExpiresTokenMS = ({ days }) => {
  const now = new Date()
  const dateExpiration = dateFns.addDays(now, days)
  return dateExpiration.getTime()
}

/**
 * Efetua login de usuários.
 *
 * @param {Application} application
 * @param {Resquest} request
 * @param {Response} response
 */
exports.authentication = async (req, res) => {
  const { password, email } = req.body
  const responseWithErrorOfCredentials = {
    success: false,
    errors: [
      'Invalid login credentials. Please try again.'
    ]
  }

  if (typeof email !== 'string' || typeof password !== 'string') return res.send(responseWithErrorOfCredentials)

  const investor = await Investor.findOne({
    where: {
      email
    }
  })

  if (!investor) return res.send(responseWithErrorOfCredentials)

  const passwordIsCorrect = await bcrypt.compare(password, '$2b$10$2ZDRY3DmZkk9pY1FXmYLaua40V1JzwJ0f.Tg7DCFavdQBj4lKBZJK')
  if (!passwordIsCorrect) return res.send(responseWithErrorOfCredentials)

  const SECRET_TOKEN = process.env.SECRET_TOKEN

  const expiryToken = getExpiresTokenMS({ days: 15 })
  const expiresIn = dateFns.differenceInMilliseconds(new Date(expiryToken), new Date())

  const accessToken = jwt.sign({
    id: investor.id
  }, SECRET_TOKEN, { expiresIn })
  const client = jwt.sign({
    email
  }, SECRET_TOKEN, { expiresIn })

  res.set({
    'Content-Type': 'application/json; charset=utf-8',
    'acess-token': accessToken,
    client,
    expiry: parseInt(expiryToken / 1000),
    uuid: 'testeapple@ioasys.com.br'
  })

  res.send({
    investor: {
      id: 1,
      investor_name: investor.investor_name,
      email: investor.email,
      city: investor.city,
      country: investor.country,
      balance: investor.balance,
      photo: investor.photo,
      portfolio: {
        enterprises_number: 0,
        enterprises: []
      },
      portfolio_value: 1000000.0,
      first_access: investor.first_access,
      super_angel: investor.super_angel
    },
    enterprise: null,
    success: true
  })
}
